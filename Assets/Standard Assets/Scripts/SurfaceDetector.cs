﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Class for detecting on what surface the vehicle is going be on.
/// </summary>
public class SurfaceDetector : MonoBehaviour {

    public Transform Indicator;
    public GUISkin skin;
    bool onTrack = true;
    bool checkpoint = false;

    int kierrokset;
    static bool record;
    static float currentTime;
    static float bestTime;
    

    /// <summary>
    /// Initialization steps.
    /// </summary>
    void Start () {

        bestTime = 1000.0f;
        currentTime = 0;

	}
	
	/// <summary>
	/// Update steps.
	/// </summary>
	void Update () {

		RaycastHit objectHit;        

		// Draw the detector ray.
		Debug.DrawRay(transform.position, transform.forward * 50f, Color.green);

		// Check what the ray hit.
		if (Physics.Raycast(transform.position, transform.forward, out objectHit, 500)) {
            if (objectHit.collider.gameObject.tag == "track")
            {
                checkpoint = false;
                onTrack = true;
                Debug.Log("We are approching " + objectHit.collider.gameObject.tag);
            }
            else if (objectHit.collider.gameObject.tag == "offtrack")
            {
                if(GameObject.FindGameObjectsWithTag("Indicator").Length < 1)
                {
                    Instantiate(Indicator, this.gameObject.transform.position, this.gameObject.transform.rotation);
                }
                onTrack = false;
                checkpoint = false;
                Debug.Log("We are approching " + objectHit.collider.gameObject.tag);
            }
            else if (objectHit.collider.gameObject.tag == "checkpoint")
            {
                onTrack = true;
                checkpoint = true;
                Debug.Log("We are approching " + objectHit.collider.gameObject.tag);
            }
            else if (objectHit.collider.gameObject.tag == "Indicator")
            {
                Debug.Log("INDIKAATTORI LÖYTYNYT");
                Destroy(GameObject.FindWithTag("Indicator"));
                
            }

            else
                Debug.Log("No idea what is ahead");

		}
        if (record)
            currentTime += 1 * Time.deltaTime;

	}
    static void StartRecord()
    {
        record = true;
    }
    static void StopRecord ()
    {
        record = false;
        if ( currentTime < bestTime)
        {
            bestTime = currentTime;
        }
    } 
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Viiva")
        {
            currentTime = 0;
            StartRecord();
            kierrokset++;
        }
        if (other.tag == "Maali")
        {
            StopRecord();
        }
        

    }
  
    
    void OnGUI()
    {
        GUI.skin = skin;
        if (onTrack == false)
        {
            GUI.Label(new Rect(10, 10, 100, 90), "PALAA RADALLE");
        }
        if (onTrack == true && checkpoint == true)
        {
            GUI.Label(new Rect(10, 10, 100, 90), "CHECKPOINT");
        }
        GUI.Label(new Rect(Screen.width - 100, 0, 100, 50), "Kierrokset: " + kierrokset);
        GUI.Label(new Rect(Screen.width -100, 40, 100, 50), "Kierrosaika: " + currentTime );
        GUI.Label(new Rect(Screen.width - 100, 80, 100, 50), "Paras aika: " + bestTime);

    }
}
